module gitlab.com/AdrienneCohea/terraform-provider-vaultlifecycle

go 1.13

require (
	github.com/hashicorp/terraform-plugin-sdk v1.4.0
	github.com/hashicorp/vault/api v1.0.4
	github.com/mdempsky/gocode v0.0.0-20191202075140-939b4a677f2f // indirect
	golang.org/x/tools v0.0.0-20191206204035-259af5ff87bd // indirect
)
