package main

import (
	"log"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	vault "github.com/hashicorp/vault/api"
)

func resourceVaultInitialization() *schema.Resource {
	return &schema.Resource{
		Create: vaultInitializationCreate,
		Read:   vaultInitializationRead,
		Update: vaultInitializationUpdate,
		Delete: vaultInitializationDelete,

		Schema: map[string]*schema.Schema{
			"address": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "https://127.0.0.1:8200",
			},
		},
	}
}

func vaultInitializationCreate(d *schema.ResourceData, m interface{}) error {
	client, err := vault.NewClient(&vault.Config{
		Address: d.Get("address").(string),
	})

	if err != nil {
		log.Fatalf("Unable to initialize Vault client: %s", err)
	}

	initialized, err := client.Sys().InitStatus()

	if err != nil {
		log.Fatalf("Unable to check Vault initialization status: %s", err)
	}

	if initialized {
		return vaultInitializationRead(d, m)
	} else {
		init, err := client.Sys().Init(&vault.InitRequest{
			SecretShares: d.Get("secret_shares").(int),
		})

		if err != nil {
			log.Fatalf("Unable to initialize Vault: %s", err)
		}

		d.Set("keys", init.Keys)
		d.Set("keys_b64", init.KeysB64)
		d.Set("recovery_keys", init.RecoveryKeys)
		d.Set("recovery_keys_b64", init.RecoveryKeysB64)
		d.Set("root_token", init.RootToken)
	}

	return nil
}

func vaultInitializationRead(d *schema.ResourceData, m interface{}) error {
	return nil
}

func vaultInitializationUpdate(d *schema.ResourceData, m interface{}) error {
	return nil
}

func vaultInitializationDelete(d *schema.ResourceData, m interface{}) error {
	return nil
}
